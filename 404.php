<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package wptest
 * @since v.0
 * @author  Danilo B. Matias Jr.  
 * @author URI danilomatias.bid
 */

get_header(); ?>

<h1>404 Error</h1>
We cannot seem to find what you were looking for.
Maybe we can still help you.
<ul>
	<li>You can search our site using the form provided below.</li>
	<li>You can visit <a href="<?php bloginfo?>"</a></li>
	<a href="<?php ('url'); ?>" the homepage.</a>
	<li>Or you can view some of our recent posts.</li>
</ul>

Search:

TEMPLATEPATH . "/searchform.php"); ?>

<h3>Recent Posts</h3>

<ul>
	<?php 
		query_posts('posts_per_page=5'); 
		if (have_posts()) : 
			while (have_posts()) : 
				the_post(); 
	?>
					<li><a href="<?php the_permalink() ?>" title="Permalink for : <?php the_title(); ?>"><?php the_title(); ?></a></li>
	<?php
			endwhile; 
		endif; 
	?>
</ul>

<?php get_footer(); ?>