<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage wptest
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<!DOCTYPE html>
<html <?php language_attributes(); ?> >
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>WPTEST</title>
	<?php wp_head(); ?>
</head>
<?php
	global $post;
?>
<body <?php body_class( explode( '/', get_page_link( $post->ID ) ) [3] ); ?>>
	<header>
		<nav class="navbar navbar-expand-md navbar-light fixed-top navbarcustom space-p-one-sides" role="navigation">
			<!-- <div class="container"> -->
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="container">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
				 aria-controls="bs-example-navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<img src="<?php echo esc_url( get_template_directory_uri() ) . '/assets/img/namesforbabbies_final1.png'; ?>" alt="Logo">
				</a>
				<?php
					wp_nav_menu(
						array(
							'theme_location'  => 'primary',
							'depth'           => 1,
							'container'       => 'div',
							'container_class' => 'collapse navbar-collapse',
							'container_id'    => 'bs-example-navbar-collapse-1',
							'items_wrap'      => '<ul id="%1$s dbmnav_wrap" class="%2$s">%3$s</ul>',
							'menu_class'      => 'nav navbar-nav ml-auto',
							'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
							'walker'          => new WP_Bootstrap_Navwalker(),
						)
					);
				?>
			</div>
		</nav>
	</header>
<p>sxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx</p>
