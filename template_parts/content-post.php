<?php
/**
 * Template for displaying single post from file single.php to template part post.
 *
 * @package wptest
 * @version 1.0
 * @since 2018
 */

?>
<div class="container">
	<div class="row space-m-7-top">
	<div class="col-xs-12 space-p-three bgwhite rounded-corners-one">
			<h4><?php the_title(); ?></h4>
			<p><?php the_content(); ?></p>
		</div>
	</div>
</div>
