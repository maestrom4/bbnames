<?php
/**
 * Template for displaying post pages from page.php to template parts folder reading filename content-page.php.
 *
 * @package wptest
 * @version 1.0
 * @since 2018
 */

?>
<article>
	<div class="container space-m-7-top">
		<div class="row">
			<div class="col-xs-12 space-p-three bgwhite rounded-corners-one width-full">
				<h4>
					<?php echo the_title(); ?>
				</h4>
				<p>
					<?php echo esc_url( get_the_content() ); ?>
				</p>
			</div>
		</div>
	</div>
</article>
