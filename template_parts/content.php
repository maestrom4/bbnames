<?php
/**
 * Main Post content for displaying blog post
 *
 * @package wptest
 * @since 2018
 * @version 1.0
 */

?>
<!-- <div class="container"> -->
<div class="row">
	<div class="col-lg-12 space-p-three-topdown space-p-one">
		<?php
		// Check wether page is frontpage or home if not adds title!
		if ( is_front_page() || is_home() ) {
			echo '<p>' . the_content() . '</p>';
		} else {
			echo '<h4 class="space-m-two-topdown">' . the_title() . '</h4>';
			echo '<p>' . the_content() . '</p>';
		}
		?>
	</div>
</div>
