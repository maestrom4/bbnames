<?php 

/**
 * Creates custom widget for categories list.
 */
class Wptest1 extends WP_Widget {
	/**
	 * PHP5 constructor.
	 *
	 * @since 2.8.0
	 */
	function __construct() {
		parent::__construct(
			// Base ID of your widget.
			'wptest1',
			// Widget name will appear in UI.
			__( 'Wptest', 'wptest1_domain' ),
			// Widget description.
			array( 'description' => __( 'Widget for displaying tabled categories', 'wptest1_domain' ) )
		);
	}

	/**
	 * Echoes the widget content.
	 *
	 * Sub-classes should over-ride this function to generate their widget code.
	 *
	 * @since 2.8.0
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance The settings for the particular instance of the widget.
	 */
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		echo '<h5 class="widget-title">' . esc_html__( 'Archives', 'wptest1' ) . '</h5>';
		echo '<table class="table">';
		echo '<tbody>';
		$args = array(
			'type'            => 'alpha',
			'limit'           => '5',
			'format'          => 'html',
			'before'          => '<tr><td>',
			'after'           => '</td></tr>',
			'show_post_count' => false,
			'echo'            => 1,
			'order'           => 'DESC',
			'post_type'       => 'post',
		);
		wp_get_archives( $args );

		echo '</tbody>';
		echo '</table>';
		echo '<img class="img-responsive" src="' . esc_html( get_template_directory_uri() ) . '\'/assets/img/fakeads_final2.jpg\'" alt="">';
	}

	/**
	 * Widget Backend configurations.
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {
		if ( isset( $instance['title'] ) ) {
			$title = $instance['title'];
		} else {
			$title = __( 'New title', 'wptest1_domain' );
		}
		// Widget admin form.
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<?php
	}
	/**
	 * Updates a particular instance of a widget.
	 *
	 * This function should check that `$new_instance` is set correctly. The newly-calculated
	 * value of `$instance` should be returned. If false is returned, the instance won't be
	 * saved/updated.
	 *
	 * @since 2.8.0
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Settings to save or bool false to cancel saving.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}
}