<?php
/**
 * This is our main file for our theme. The default fallback for each template file.
 *
 * @package wp_test
 * @author Danilo B. Matias Jr.
 * @version 1.0
 * @since 2018
 */

get_header();
if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
			get_template_part( 'template_parts/content', 'get_posts_format' );
	endwhile;
endif;
get_sidebar();
get_footer();
