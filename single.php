<?php
/**
 * This is our template for displaying all our single post.
 *
 * @link www.danilomatias.bid
 * @package wptest1
 * @version 1.0
 * @since 2018
 */

get_header();
if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
			get_template_part( 'template_parts/content', 'post' );
	endwhile;
endif;

get_footer();

