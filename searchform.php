<?php
/**
 * Template for search form reference can be found at general-template.php with instructions.
 * Here you can overide default widget search form.
 *
 * @package wptest1
 * @version 1.0
 * @since 2018
 * */

echo '<form role="search" method="get" class="search-form form-inline" action="' . esc_url( home_url( '/' ) ) . '">
        <div class="form-group">
            <span class="screen-reader-text">' . esc_html( 'Search for:', 'label' ) . '</span>
            <input type="search" class="col-sm-12 search-field" placeholder="' . esc_attr_x( 'Search &hellip;', 'placeholder' ) . '" value="' . get_search_query() . '" name="s" />
            </label>
            <button type="submit" class="col-sm-2 space-p-zero sidebar-search-btn"><span class="dashicons dashicons-search"></span></button>
         </div>
    </form>';

