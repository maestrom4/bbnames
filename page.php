<?php
/**
 * This is page file for our theme, this file select which template content does a page post use.
 *
 * @package wp_test
 * @author Danilo B. Matias Jr.
 * @version 1.0
 * @since 2018
 */

get_header();
if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		get_template_part( 'template_parts/content', 'page' );
	endwhile;
endif;
get_footer();
