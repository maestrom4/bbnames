<?php
/**
 * This is our sidebar file.
 *
 * @link www.danilomatias.com refer to sidebar template on wordpress.org
 * @package wptest1
 * @version 1.0
 * @since 2018
 */

?>

<?php
if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
if ( ! is_active_sidebar( 'sidebar-2' ) ) {
	return;
}

?>

<aside id="search" class="widget-area front-page-sidebar space-p-one space-m-zero " role="complementary" aria-label="<?php esc_attr_e( 'Sidebar', 'wp test' ); ?>">
	<ul>
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	</ul>
</aside>
<aside id="categories" class="widget-area front-page-sidebar space-p-one space-m-zero " role="complementary" aria-label="<?php esc_attr_e( 'Sidebar', 'wp test' ); ?>">
	<table class="table">
		<?php dynamic_sidebar( 'sidebar-2' ); ?>
	</table>
</aside>
