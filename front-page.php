<?php
/**
 * Front page template for displaying homepage if super admin set page to static page
 *
 * @package wptest
 * @since 2018
 * @version 1.0
 */

?>
<?php get_header(); ?>
<section class="container-fluid space-p-zero">
	<div id="carouselId" class="carousel-fluid slide" data-ride="carousel">
		<div class="carousel-inner" role="listbox">
			<div class="carousel-item active">
				<img src="<?php echo esc_url( get_template_directory_uri() ) . '/assets/img/slider1.jpg'; ?>" alt="First slide">
			</div>
			<div class="carousel-item">
				<img src="<?php echo esc_url( get_template_directory_uri() ) . '/assets/img/slider2.jpg'; ?>" alt="Second slide">
			</div>
			<div class="carousel-item">
				<img src="<?php echo esc_url( get_template_directory_uri() ) . '/assets/img/slider3.jpg'; ?>" alt="Third slide">
			</div>
			<div class="carousel-item">
				<img src="<?php echo esc_url( get_template_directory_uri() ) . '/assets/img/slider4.jpg'; ?>" alt="Fourth slide">
			</div>
		</div>
		<a class="carousel-control-prev" href="#carouselId" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#carouselId" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
</section>

<section id="rowctrl" class="container space-m-negative-one-top space-m-zero-side">
	<ul class="row">
		<li id="box1" class="col-lg-3 box text-color-boxes">
			<a href="<?php echo esc_url( get_site_url() ) . '/unique'; ?>">
				<h1 class="text-color-boxes space-m-zero">Unique</h1><span>name</span>
			</a>
		</li>
		<li id="box2" class="col-lg-3 box text-color-boxes">
			<a href="<?php echo esc_url( get_site_url() ) . '/modern'; ?>">
				<h1 class="text-color-boxes space-m-zero">Modern</h1><span>name</span>
			</a>
		</li>
		<li id="box3" class="col-lg-3 box text-color-boxes">
			<a href="<?php echo esc_url( get_site_url() ) . '/boy'; ?>">
				<h1 class="text-color-boxes space-m-zero">Name</h1><span><span>for</span>
					<h2 class="text-color-boxes"><b>Babby Boy</b></h2>
				</span>
			</a>
		</li>
		<li id="box4" class="col-lg-3 box text-color-boxes">
			<a href="<?php echo esc_url( get_site_url() ) . '/girl'; ?>">
				<h1 class="text-color-boxes space-m-zero">Name</h1><span><span>for</span>
					<h2 class="text-color-boxes"><b>Babby Girl</b></h2>
				</span>
			</a>
		</li>
	</ul>
</section>
<section id="rowctrl2" class="container">
	<div class="row space-m-one-topdown">
		<article class="col-lg-9 space-p-zero-sid front-page-content space-m-one-topdown">
			<div class="col-lg-12">
				<?php
				if ( have_posts() ) :
					while ( have_posts() ) :
						the_post();
						get_template_part( 'template_parts/content', 'get_posts_format' );
					endwhile;
				endif;
				?>
			</div>
		</article>
		<aside class="col-lg-3 space-m-one-topdown">
			<?php get_sidebar( 'search' ); ?>
		</aside>
	</div>
</section>
<?php get_footer(); ?>
