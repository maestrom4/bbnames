<?php
/**
 * Functions file for configuring wp enques,action, hooks etc.
 *
 * @package wp_test
 * @author Danilo B. Matias Jr.
 * @version 1.0
 * @since 2018
 */

/**
 * Registers theme support for Wordpress
 */
if ( ! function_exists( 'danmats_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function danmats_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on danmats, use a find and replace
		 * to change 'danmats' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'danmats', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'danmats' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'danmats_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'danmats_setup' );

/**
 * Loads all scripts and stylesheets of the theme.
 */
function wptest_scripts() {
	/* Loading Stylesheet!  */
	wp_enqueue_style( 'stylesheet', get_stylesheet_directory(), array(), true );
	wp_enqueue_style( 'dashicon-style', get_stylesheet_uri(), array( 'dashicons' ), true );
	wp_register_style( 'bootstrap_css', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), true );
	wp_enqueue_style( 'bootstrap_css' );
	wp_register_style( 'custom_css', get_template_directory_uri() . '/assets/css/custom.css', array(), true );
	wp_enqueue_style( 'custom_css' );
	/* Loading Scripts!  */
	wp_register_script( 'bootstrap_js', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array( ' jquery ' ), true, true );
	wp_enqueue_script( 'bootstrap_js' );
	wp_register_script( 'custom_js', get_template_directory_uri() . '/assets/js/custom.js', array(), true, true );
	wp_enqueue_script( 'custom_js' );
}

add_action( 'wp_enqueue_scripts', 'wptest_scripts' );
require_once get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php';
require_once get_template_directory() . '/inc/class-wptest1.php';


/**
 * Registers sidebar widgets for additional theme functionality.
 */
function customtheme_widget() {
	// Register sidebar 1 for search or other widgets.
	register_sidebar(
		array(
			'name'          => sprintf( __( 'sidebar-1' ), 'customtheme' ),
			'id'            => 'sidebar-1',
			'description'   => '',
			'class'         => '',
			'before_widget' => '<li id="%1$s" class="widget %2$s space-m-one-topdown">',
			'after_widget'  => "</li>\n",
			'before_title'  => '<h5 class="widgettitle space-m-onep5-topdown">',
			'after_title'   => "</h5>\n",
		)
	);
	// Register sidebar 2 for categories or other widgets.
	register_sidebar(
		array(
			'name'          => sprintf( __( 'sidebar-2' ), 'customtheme2' ),
			'id'            => 'sidebar-2',
			'description'   => '',
			'class'         => 'sidebar-2',
			'before_widget' => '<div id="%1$s" class="widget %2$s space-m-one-topdown space-p-one-topdown">',
			'after_widget'  => "</div>\n",
			'before_title'  => '<h5 class="widgettitle space-m-onep5-topdown">',
			'after_title'   => "</h5>\n",
		)
	);
	// Register and load the custom widget for categories with a bootsrap wrapper.
	register_widget( 'wptest1' );
}
add_action( 'widgets_init', 'customtheme_widget' );
