<?php
/**
 * Footer template for displaying footer area, admin bar, and other information.
 *
 * @package wptest
 * @version 1.0
 * @since 2018
 */

?>
<footer id="site-footer" class="space-m-5-top">
	<div class="container">
		<div class="row space-p-one-sides">
			<div class="col-xs-2 col-md-2 footer-five">
				<h5 class="line-bottom">Our Parters</h5>
				<ul>
					<li>
						<h5>Winners</h5>
						<p>Lorem ipsum, dolor ipsum, dolor ipsum, dolor. </p>
					</li>
					<li>
						<h5>Lossers</h5>
						<p>Lorem ipsum dolor sit, amet consect.ipsum dolor sit, amet consect</p>
					</li>
				</ul>
				<p>
					<a href="#">Birthday Coordinators</a>
					<a href="#">Party Maker</a>
				</p>
			</div>
			<div class="col-xs-2 col-md-2 footer-five">
				<h5 class="line-bottom">Categories</h5>
				<ul>
					<li>Girl's names</li>
					<li>Boy's names</li>
					<li>Smart names</li>
					<li>Influencial names</li>
					<li>Names of the rich</li>
					<li>Saint's names</li>
				</ul>
			</div>
			<div class="col-xs-2 col-md-2 footer-five">
				<h5 class="line-bottom">Sponsors</h5>
				<ul>
					<li><a href="#">Nemo impedit</a></li>
					<li><a href="#">Consectetur illo</a></li>
					<li><a href="#">Reiciendis vitae</a></li>
					<li><a href="#">exercitationem</a></li>
					<li><a href="#">Quisquam quas</a></li>
					<li><a href="#">Cum omnis</a></li>
				</ul>
			</div>
			<div class="col-xs-2 col-lg-2 footer-five">
				<h5 class="line-bottom"><a href="<?php echo esc_url( site_url() ); ?>/contact/">Contact us</a></h5>
				<p>
					Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa voluptatum doloremque impedit.
				</p>
				<h6><a href="#">dnlmts96@gmail.com</a></h6>
				<h6>+639551530099</h6>
			</div>
			<div class="col-xs-2 col-lg-2 footer-five">
				<h5 class="line-bottom">Our Socialmedia</h5>
				<a href="#"><span class="dashicons dashicons-facebook space-m-zero-p5-sides"></span></a>
				<a href="#"><span class="dashicons dashicons-googleplus space-m-zero-p5-sides"></span></a>
				<a href="#"><span class="dashicons dashicons-twitter space-m-zero-p5-sides"></span></a>
			</div>
		</div>
	</div>
</footer>
<?php // wp_footer(); ! ?>
</body>
</html>
